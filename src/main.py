from Voice import voice
from NLG import nlgMasterFile
from NLP import intentClassification


def main(inputString):
    voice.cleanspeak(nlgMasterFile.generateResponse(intentClassification.intentClassification(inputString)))
    
while True:
    main(input("What do you want to say to me?: "))
