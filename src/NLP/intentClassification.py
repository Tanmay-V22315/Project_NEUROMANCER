#This is perhaps the smallest python file that drives the main execution but it sure was a PITA to train the model and everything.

from transformers import AutoTokenizer, TFAutoModelForSequenceClassification
import os
import time
from json import load

#Get directory of this file
dir_path = os.path.dirname(os.path.realpath(__file__))

#All the classes have been stored in a json file and that is being loaded and assigned as a dictionary for further use
try:
    resources=load(open(dir_path+"/resources.json"))
    classes=resources['classes']
except:
    raise FileNotFoundError("resources.json was not found or is not properly formatted, so classes were not loaded. Execution beyond this point is not possible and will, therefore, be terminated.")

print("Loading roBERTa intent classification model. This can take a while if this is the first time since startup. please wait.....")
try:
  model=TFAutoModelForSequenceClassification.from_pretrained(dir_path+"/model")
except:
  raise FileNotFoundError("The intent classification model is either invalid or missing. Execution beyond this point is not possible and will, therefore, be terminated.")

try:
  tokenizer = AutoTokenizer.from_pretrained("google/electra-base-discriminator")
except:
  raise FileNotFoundError("The Tokenizer cannot be loaded from huggingface. Execution beyond this point is not possible and will, therefore, be terminated.")

def pad(inputlist:list, padLength:int):
  return inputlist+[0]*(padLength-len(inputlist))
  
def textToids(inputString:str):
  return pad(tokenizer.convert_tokens_to_ids(["[CLS]"]+tokenizer.tokenize(inputString)+["[SEP]"]), 59)




def intentClassification(inputString: str, mode='release'):
    time1=time.perf_counter()
    prediction=model.predict([textToids(inputString)])[0]
    classifiedAs=classes[prediction[0].argmax(axis=-1)]
    confidence=max(prediction[0])*10
    time2=time.perf_counter()
    if mode=='debug':
        print("__"*45)
        print("-> Classified As: "+str(classifiedAs))
        print("-> Confidence: "+str(confidence))
        print("Time taken for inference: "+str(time2-time1))
        print("__"*45)
    return [inputString, confidence, classifiedAs]