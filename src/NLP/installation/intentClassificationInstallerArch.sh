echo "--------------------------------------------------------------------------"
echo "    _   ____    __  __"
echo "   / | / / /   / / / /"
echo "  /  |/ / /   / / / / "
echo " / /|  / /___/ /_/ /  "
echo "/_/ |_/_____/\____/   "
echo "--------------------------------------------------------------------------"

echo "Synchronizing Package databases......"
sudo pacman -Syu
echo "Done!"

echo "installing python modules....."
pip3 install bert-for-tf2 
pip3 install sentencepiece
echo 
echo
echo "WARNING: Tensorflow 2.3.1 will now be installed. This may cause some conflicts with other modules or packages since the latest version is 2.5 (as of writing this script). Version 2.3.1 is being installed because loading an hdf5 model with a custom layer seems to be broken in the latest version (2.5), but it works in 2.3.1"
echo "Do you wish to proceed?  [y|n]..."
read consent

if [ "$consent" = "y" ]
    then
        pip3 install tensorflow==2.3.1
elif [ "$consent" = "Y" ]
    then
        {pip3 install tensorflow==2.3.1}
else
    echo "Understood stopping now."
fi