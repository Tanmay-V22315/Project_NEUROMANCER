#TODO allow for installing additional mods to literaryBrain
from zero_shot_clf import clf
from sentence_punct import sentence_punct
from qa_model import qa

def intermediate(query:dict):
    if query['type']=='punctuation':
        return sentence_punct.main(query)

    elif query['type']=='qa':
        return qa.main(query)

    elif query['type']=='clf':
        return clf.main(query)

    else:
        raise "Required query type cannot be handled. Stopping execution to prevent undefined behaviour."
