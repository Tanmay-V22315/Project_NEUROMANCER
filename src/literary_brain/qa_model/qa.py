from transformers import pipeline
qa=pipeline('question-answering',model="deepset/roberta-base-squad2")

def main(query:dict):
    try:
        return qa(query['question'],query['context'])
    except:
        raise "Query was formatted incorrectly. Please ensure that 'question' and 'context' keys exist in the query dictionary, both keys having string value. Stopping execution to prevent undefined behaviour"