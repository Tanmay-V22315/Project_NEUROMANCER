from transformers import pipeline
classifier=pipeline("zero-shot-classification",model="valhalla/distilbart-mnli-12-3")

def main(query:dict):
    try:
        return classifier(query['sentence'],query['candidate_labels'])
    except:
        raise "Query was formatted incorrectly, there must be a key called 'sentence' and another key called 'candidate_labels' and the former's value must be a string while the latter's value has to be a list. Stopping execution to prevent undefined behaviour"