from simpletransformers.t5 import T5Model
try:
    model=T5Model("t5-base","./",max_length=600)
except:
    model=T5Model("t5-base","./",use_cuda=False, max_length=600)

def main(query:dict):
    try:
        return model.predict("add punctuation to sentence:",query['sentence'])
    except:
        raise "Incorrect formatting of query. query must have a key named 'sentence' with value being the sentence you want fixed. Stopping execution to prevent undefined behaviour"
