echo "NOTE: The more advanced functionalities of the 'distance' mod requires the usage of Mapbox API which requires an API key."

echo "You can obtain the API key (Access Token) by simply signing up at mapbox.com and generate an access token."

echo "ENTER YOUR ACCESS TOKEN HERE:"

read $mapboxAT

export MAPBOX_ACCESS_TOKEN=$mapboxAT