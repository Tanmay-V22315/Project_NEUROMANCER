#TODO Account for following types of greets:
# 1. Standard: Hello
# 2. Status: Hello, how are you?
# 3. Formal Greetings: Good Morning, Good afternoon....you get the idea
# 4. Casual: Yo, G'day(This particular one won't be found in the wild, but just in case)

import json
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
f=open(dir_path+"/cache.json","r+")
x = json.load(f)
f.seek(0)
f.close()
f=open(dir_path+"/cache.json","w")






from datetime import datetime
from random import choice
from spacy import load



now = datetime.now()
hour=int(now.strftime("%H"))
nlp = load("en_core_web_sm")


def main(inputString, timeHour=int(now.strftime("%H")), someDict=x, f=f):
    """Classify the input into any one of these. Every type of greet can't be replied with 'Hello' or 'How are you' or something (btw, I just figured out triple quotes comments, they're pretty neat ig)"""
    doc = nlp(inputString)
    posList=[]

    for token in doc:
        posList.append(token.pos_)
    print(posList)

    if someDict['counter']>1:
        return choice(["I thought we already greeted each other.", "You know, you do realize that I'm not a simple chatbot that says hello everytime you say hello, right?","Am I having dementia or something?", "Oh dear, You alright?"])

    if "PRON" in posList and "ADV" in posList or "how" in inputString.lower():
        if someDict['counter']==0:
            someDict['counter']=someDict['counter']+1
            return choice(["I'm fine, how about you?", "I'm fine, I'm fine. So.<pause>0.2</pause> how about you? How are you doing?"])
        elif someDict['counter']==1:
            return ["I'm fine.", "Hi."]

    elif "PRON" in posList and "ADJ" in posList:
        return choice(["That's good to know.", "Nice.", "Cool.", "Nice. So what do you want to talk about?"])

    elif posList==['INTJ', 'PUNCT']:
        return choice(["Hello, how are you?", "Hey, how are you today?", "Hi.", "Hello."])
    
    elif posList==["ADJ",'NOUN','PUNCT'] and "good" in inputString.lower():
        if 0<=timeHour<10:
            dayStatus="Morning."
        elif 10<=timeHour<14:
            dayStatus="Afternoon."
        else:
            dayStatus="Evening."
        return "Good"+" "+str(dayStatus)
    
    elif posList==['PROPN', 'PUNCT']:
        return choice(["Heya.", "Yo.", "How you doin?", "How are ya?"])

    else:
        return "Hello."
    
    json.dump(x, f, indent=4)
    f.close()





#BOT                        #HUMAN
#1.                     
#             
#Hello
#                          Hello, how are you?
#I'm fine. How about you?
#                          I'm fine too.
#Cool
#                           Tell me something about.....
#____________________________________________________
# 2.
#
#Hello.
#                           Hi.
#How are you?
#                           I'm fine, how about you?
#I'm fine too.
#                           Cool.
#
#