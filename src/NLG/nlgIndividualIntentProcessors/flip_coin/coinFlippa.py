from random import SystemRandom

def main(*_):
    coinFlipResult=SystemRandom().randrange(2)
    if coinFlipResult==1:
        return "Heads."
    else:
        return "Tails"
