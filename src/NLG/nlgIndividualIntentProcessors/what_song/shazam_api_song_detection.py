import os
import json
import base64 
import requests
import sounddevice as sd
import time
from soundfile import write
dir_path= os.path.dirname(os.path.realpath(__file__))

def main(*_):
    #Recording audio from microphone
    x=sd.rec(7*44100,44100, channels=1)
    sd.wait()

    #writing it to a temporary file so that it can be loaded later
    write(dir_path+"/temp.wav", x, 44100)
    enc=base64.b64encode(open(dir_path+"/temp.wav","rb").read())

    url = "https://shazam.p.rapidapi.com/songs/detect"

    payload = enc
    api_key=os.getenv("RAPIDAPI_SHAZAM_KEY")
    headers = {
        'content-type': "text/plain",
        'x-rapidapi-host': "shazam.p.rapidapi.com",
        'x-rapidapi-key': api_key
        }

    response = requests.request("POST", url, data=payload, headers=headers)

    #Cleaning up
    os.remove(dir_path+"/temp.wav")

    return_message="Identified as: "+json.loads(response.text)['track']['title']+" by "+json.loads(response.text)['track']['subtitle']
    if return_message!=None:
        return return_message
    else:
        return "I could not find a match."

    