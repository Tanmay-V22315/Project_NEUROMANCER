#TODO Averaged Perceptron to be included during setup
from nltk.tag import pos_tag 
from nltk.tokenize import word_tokenize
from bs4 import BeautifulSoup
from requests import get
from wikipedia import summary
import warnings
import re

def keywordExtraction(someString):
    outputSentence=''
    tokens=pos_tag(word_tokenize(someString))
    counter=0
    for i in tokens:
        try:
            if i[1]=="NNP":
                counter+=1
                outputSentence=outputSentence+" "+i[0]
                if tokens[tokens.index(i)+1][1]=="NN":
                    outputSentence=outputSentence+" "+str(tokens[tokens.index(i)+1][0])
                elif tokens[tokens.index(i)+1][1]=="CD":
                    outputSentence=outputSentence+" "+str(tokens[tokens.index(i)+1][0])
        except:
            warnings.warn("Some weird word or character was found; Please contact author and report it.", UnknownHandler)
            pass
    #Just to make sure something is passed through the thing, incase capitalization doesn't work or something.
    if counter==0:
        outputSentence=''
        for i in tokens:
            try:
                if i[1]=="NN":
                    outputSentence=outputSentence+i[0]
                    if tokens[tokens.index(i)+1][1]=="CD":
                        outputSentence=outputSentence+" "+str(tokens[tokens.index(i)+1][0])
            except:
                warnings.warn("Some weird word or character was found; Please contact author and report it. Along with the input query: "+str(someString), UnknownHandler)
                pass
    
    return re.sub("[\\[].*?[\\]]", "", outputSentence.strip().title())

def WikiScraper(inputTerm):
    try:
        source=get("https://simple.wikipedia.org/wiki/"+inputTerm.replace(" ", "_"))
        soup=BeautifulSoup(source.text,"html.parser")
        parentDiv=soup.find('div', class_="mw-parser-output")
        result=''
        for paragraph in parentDiv.find_all('p')[0:2]:
            result=result + paragraph.text
        result = result.strip()
        assert result!=None or result.endswith("may refer to:")
        return result
    except:
        try:
            source=get("https://en.wikipedia.org/wiki/"+inputTerm.replace(" ", "_"))
            soup=BeautifulSoup(source.text,"html.parser")
            parentDiv=soup.find('div', class_="mw-parser-output")
            result=''
            for paragraph in parentDiv.find_all('p')[0:2]:
                result=result + paragraph.text
            result = result.strip()
            assert result!=None or result.endswith("may refer to:")
            return result
        except:
            print("Using wikipedia module")

            #This has been placed intentionally as a last-resort because the module bugs out with certain search terms like "Elon Musk" or some other ones.

            try:
                return summary(inputTerm, sentences=3)
            except:
                #This is an example of the custom SSML syntax usage (which can be expanded as well)
                return "Either I couldn't find anything about what you asked or something went wrong. Perhaps try again. <pause>0.4</pause> Or maybe try asking something else?"


def main(inputString):
    return WikiScraper(keywordExtraction(inputString))
    
