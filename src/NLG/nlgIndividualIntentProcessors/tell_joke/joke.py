from json import load
from os.path import dirname, realpath
from random import randint
from nltk import word_tokenize

dir_path = dirname(realpath(__file__))
jokesData=load(open(dir_path+"/jokes.json"))

#This function doesn't take any input because unlike the greeter, in whatever obtuse manner you ask for it, only a joke can be returned
def main(*_):
    jokeNum=randint(0,len(jokesData))
    joke=jokesData[jokeNum]['body'].replace('"',"").strip().replace("\n","").replace("!",".")
    jokeRating=jokesData[jokeNum]['rating']
    jokeType=jokesData[jokeNum]['category']

    if joke=="" or len(word_tokenize(joke))>30 or jokeRating<2.5 or joke==None or jokeType in ["Insults", "Yo Mama"]:
        return main()
    else:
        return joke

