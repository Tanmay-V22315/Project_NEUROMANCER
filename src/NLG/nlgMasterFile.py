# This file is an interface to the rest of the intent handling files. This was done so that only the file(s) required are called while also making it easy to add more intents (If you did that with the intent classifier as well)
import warnings
import hjson
import os
import sys


dir_path = os.path.dirname(os.path.realpath(__file__))
dir_pathIntents = os.path.dirname(os.path.realpath(__file__))+"/nlgIndividualIntentProcessors"

intentConfigs={}

for item in os.listdir(dir_pathIntents):
    if os.path.isdir(dir_pathIntents+"/"+item):
        sys.path.append(dir_pathIntents+"/"+item)
        temp=hjson.load(open(dir_pathIntents+"/"+item+"/config.hjson"))
        intentConfigs[item]=dict(temp)


#POTENTIALLY UNSAFE AND NOT SECURE: FIX AS SOON AS POSSIBLE
def generateResponse(classificationInfo):
    fileName=intentConfigs[classificationInfo[2]]['fileNameToBeExecuted']
    exec("import "+fileName)
    if intentConfigs[classificationInfo[2]]['confidenceThreshold']!=None and classificationInfo[1]<intentConfigs[classificationInfo[2]]['confidenceThreshold']:
        return intentConfigs[classificationInfo[2]]['lowConfidenceResponse']
    else:
        try:
            return eval(fileName+".main(classificationInfo[0])")
        except KeyError:
            warnings.warn("This intent cannot be handled yet, skipping")








