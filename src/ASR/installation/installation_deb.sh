#USE WORD-WRAP
#TODO(pNpm): Allow users to conveniently switch between models using a TUI interface.

pip install vosk soundfile webrtcvad
echo "Download your preferred Text-To-Speech model from https://alphacephei.com/vosk/models according to your accent/speaking style and then place the extracted files in src/ASR/model. Once you do that, you can test your model with asr_test.py in src/ASR/utils"