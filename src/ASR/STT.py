#TODO Complete ASR with vosk, webrtcvad and sounddevice by today
import sounddevice as sd
import queue
import vosk
import os

dir_path= os.path.dirname(os.path.realpath(__file__))
model=vosk.Model(dir_path+"/model")


def transcriber():
    q = queue.Queue()

    def audio_callback(indata, frames, time, status):
        q.put(bytes(indata))

    stream = sd.RawInputStream(device=None, channels=1,blocksize=8000, samplerate=44100, callback=audio_callback, dtype='int16')


    with stream:
        rec= vosk.KaldiRecognizer(model,44100)
        print("Recording")
        while True:
            data = q.get()
            if rec.AcceptWaveform(data):
                break

        return rec.FinalResult()


