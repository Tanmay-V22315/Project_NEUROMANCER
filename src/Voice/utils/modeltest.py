import sys 
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
print("Importing and evaluating voice.py file. If this is the first time since startup, this might take a while. If you're on a laptop, I recommend that you connect it to the wall.")
#import the voice.py file
import Voice.voice as voice


while 1:
    try:
        #Keep the second parameter as debug if you want details regarding inference otherwise keep it empty or type "release" (default is "release") 
        inputString=input("What do you want me to say: ")
        voice.cleanspeak(inputString, "debug")
    except KeyboardInterrupt:
        print("\nUnderstood, stopping now....")
        break



