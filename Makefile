.DEFAULT_GOAL := help
# Add the following 'help' target to your Makefile
# And add help text after each target name starting with '\#\#'
# Add project directory to PYTHONPATH
 
help:           ## Show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
 
install_arch:           ## Install dependencies and stuff for Arch-based distros (Arch, Manjaro, Garuda etc.)
	echo "It's recommended to install CUDA libraries *after* the setup is complete."
	sleep 4
	bash "./src/Voice/installation/TTSinstallationforArch_based.sh"
	bash "./src/literary_brain/installRequirements.sh"

install_deb:           ## Install dependencies and stuff for Debian-based distros (Debian, Ubuntu, Pop!OS etc.)
	echo "It's recommended to install CUDA libraries *after* the setup is complete."
	sleep 4
	bash "./src/Voice/installation/TTSinstallationforDebian_based.sh"
	bash "./src/literary_brain/installRequirements.sh"






