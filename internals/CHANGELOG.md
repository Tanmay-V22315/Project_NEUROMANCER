# *<u>Changelog:</u>*

- ## <u>Pre-release</u>
    - update .gitignore
    - edited .gitignore
    - Automatic Speech Recognition is (more or less) completed. This means the framework of the entire thing is finished, which coincidentally means we can 'go live' within a few weeks with the finished version (dev version)
    - added and populated .gitignore to prevent __pycache__ from polluting the repo
    - Merge branch 'expanding-literary-brain' into 'main'
    - Merge branch 'main' of gitlab.com:Tanmay-V22315/Project_NEUROMANCER into expanding-literary-brain Expanded literary brain section of codebase to act more like an API that can be called from any script
    - Returning after a long break. Did quite some stuff
    - Merge branch 'init' into 'main'
    - Update credits_and_sources/README.md
    - Merge branch 'master' into 'main'
    - Merge branch 'main' into 'master'
    - Initial commit
    - Reinitialized git folder due to problems
    - Initial commit
(These commits are not accurate per se)





